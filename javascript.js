//Classes
class Laptop {
    title = "";
    description = "";
    price = "";
    laptoplist = [];

    constructor(title, description, price) {
        this.title = title,
            this.description = description,
            this.price = price
    }
};

class Banker {
    balance = "";
    pay = "";
    outstandingloan = "";
    activeloan = "";

    constructor(balance, pay, outstandingloan, activeloan,) {
            this.balance = 0,
            this.pay = 0,
            this.outstandingloan = 0,
            this.activeloan = false
    }
}



//Banker Elements binding
const bankBalanceCounter = document.getElementById("bank-balance-count");
const payBalanceCounter = document.getElementById("pay-balance-count");
const outstandingLoanCounter = document.getElementById("outstanding-loan-count");
const outstandingLoanSection = document.getElementById("outstanding-loan-section");



//Laptop Elements binding
const laptopSelect = document.getElementById("laptops");
const laptopTitle = document.getElementById("laptop-title");
const laptopImage = document.getElementById("laptop-image");
const laptopDescription = document.getElementById("laptop-description");
const laptopPrice = document.getElementById("laptop-price");

//Buttons binding
const btnWork = document.getElementById("btn-work");
const btnBuyNow = document.getElementById("btn-buy-now");
const btnGetLoan = document.getElementById("btn-get-loan");
const btnBank = document.getElementById("btn-bank");



//call stack
//Make a new bankaccount
//The constructor of banker gives all instances of type banker the default property values
let JoeBanker = new Banker();
let laptop = new Laptop();
renderBalance();

const limit = JoeBanker.balance * 2;
const payTransfer =  parseInt(JoeBanker.pay);


console.log(JoeBanker.balance);

//event stack
btnWork.addEventListener('click', () => {
    finishOutstandingLoan()
});

//event stack
btnBuyNow.addEventListener('click', () => {
  
    buyLaptop();
    renderBalance()
});

//event stack
btnGetLoan.addEventListener('click', () => {
    checkForLegalLoan();
})

//event stack
btnBank.addEventListener('click', () => {
    transferPayToBalance()
})

function transferPayToBalance(){
    console.log(parseInt(JoeBanker.pay));
    JoeBanker.balance += parseInt(JoeBanker.pay);
    JoeBanker.pay = 0;
    renderPay();
    console.log(JoeBanker.balance);
    renderBalance();
}


//call stack
function checkForLegalLoan() {
    JoeBanker.outstandingloan = prompt('How much loan are you thinking of?');
    console.log(JoeBanker.outstandingloan);

    const limit = JoeBanker.balance * 2;
    console.log(parseInt(limit));


    if (JoeBanker.activeloan || JoeBanker.outstandingloan > limit ) {
        alert(`You are asking for too much now, and your maximum take is: ${limit}`);
    } 
    
    else {
        JoeBanker.activeloan = true;
        confirmLoanPayment();
    }

}

//call stack
function finishOutstandingLoan() {
    if (JoeBanker.activeloan && JoeBanker.outstandingloan >= 1) {
        JoeBanker.outstandingloan -= 100;
        console.log(JoeBanker.outstandingloan);
        renderOutstandingLoan();
    }
    else if (JoeBanker.outstandingloan === 0) {
        JoeBanker.activeloan = false;
        outstandingLoanSection.style.display = "none";
        work();
    }
}

//call stack
function confirmLoanPayment() {
    JoeBanker.balance += parseInt(JoeBanker.outstandingloan);
    console.log(JoeBanker.balance)
    renderBalance();
    renderOutstandingLoan();
}

//call stack
//increment joebankers bank balance by 100
function work() {
    JoeBanker.pay += 100;
    console.log(JoeBanker.pay);
    renderPay();
}

//call stack
function buyLaptop() {
    var balanceAfter = JoeBanker.balance - laptop.price;
    if (balanceAfter < 0) {
        alert("Your saldo is insufficient")
    } else {
        JoeBanker.balance -= laptop.price;
        
    }
    console.log(JoeBanker.balance);
    console.log(laptop.price)
}


function renderBalance() {
    bankBalanceCounter.innerHTML = `<p>${JoeBanker.balance}</p>`
}

function renderPay() {
    payBalanceCounter.innerHTML = `<p>${JoeBanker.pay}</p>`
}

function renderOutstandingLoan() {
    outstandingLoanSection.style.display = "block";
    outstandingLoanCounter.innerHTML = `<p>${JoeBanker.outstandingloan}</p>`
}

function renderLaptopPrice() {
    laptopPrice.innerHTML = `<p>${laptop.price}</p>`
}


//call stack
fetch('https://noroff-komputer-store-api.herokuapp.com/computers')

    //Event stack
    .then(function (response) {
        console.log("Response:", response);
        return response.json();
    })

    //Event Stack
    .then(function (laptops) {
      
        console.log(laptop.laptoplist)

        //convert json object list to laptop array list of objects
        for(var i in laptops)
        laptop.laptoplist.push([i, laptops [i]]
        );

        console.log(laptop.laptoplist)
        // Loop through all json laptop objects
        for (const laptopp of laptop.laptoplist) {

            for (const laptopproperty of laptopp){
            // Add json laptops to laptop class object properties
            laptop.id = laptopproperty.id;
            laptop.title = laptopproperty.title;
            laptop.price = laptopproperty.price;
            }
            
            // Add laptops by title to dropdown and give option value laptop price
            laptopSelect.innerHTML += `<option value=${laptop.price}>${laptop.title}</option>`
        }
        console.log("laptoplist",laptop.laptoplist)
    })

//Event Stack
//If laptop selected, the value will be shown, value is price of a laptop.
laptopSelect.addEventListener('change', (event) => {
console.log(event.target.value)
laptop.price = event.target.value;
renderLaptopPrice()
});
console.log(laptop.price)




